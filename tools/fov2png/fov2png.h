
// fov2png.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'pch.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols

typedef enum
{
	ePvFmtMono8 = 0,            // Monochrome, 8 bits
	ePvFmtMono16 = 1,            // Monochrome, 16 bits, data is LSB aligned
	ePvFmtBayer8 = 2,            // Bayer-color, 8 bits
	ePvFmtBayer16 = 3,            // Bayer-color, 16 bits, data is LSB aligned
	ePvFmtRgb24 = 4,            // RGB, 8 bits x 3
	ePvFmtRgb48 = 5,            // RGB, 16 bits x 3, data is LSB aligned
	ePvFmtYuv411 = 6,            // YUV 411
	ePvFmtYuv422 = 7,            // YUV 422
	ePvFmtYuv444 = 8,            // YUV 444
	ePvFmtBgr24 = 9,            // BGR, 8 bits x 3
	ePvFmtRgba32 = 10,           // RGBA, 8 bits x 4
	ePvFmtBgra32 = 11,           // BGRA, 8 bits x 4
	ePvFmtFloat32 = 12,			//Float32
	__ePvFmt_force_32 = 0xFFFFFFFF

} tPvImageFormat;

typedef enum
{
	ePvBayerRGGB = 0,            // First line RGRG, second line GBGB...
	ePvBayerGBRG = 1,            // First line GBGB, second line RGRG...
	ePvBayerGRBG = 2,            // First line GRGR, second line BGBG...
	ePvBayerBGGR = 3,            // First line BGBG, second line GRGR...
	__ePvBayer_force_32 = 0xFFFFFFFF

} tPvBayerPattern;

typedef enum
{
	ePvErrSuccess = 0,        // No error
	ePvErrCameraFault = 1,        // Unexpected camera fault
	ePvErrInternalFault = 2,        // Unexpected fault in PvApi or driver
	ePvErrBadHandle = 3,        // Camera handle is invalid
	ePvErrBadParameter = 4,        // Bad parameter to API call
	ePvErrBadSequence = 5,        // Sequence of API calls is incorrect
	ePvErrNotFound = 6,        // Camera or attribute not found
	ePvErrAccessDenied = 7,        // Camera cannot be opened in the specified mode
	ePvErrUnplugged = 8,        // Camera was unplugged
	ePvErrInvalidSetup = 9,        // Setup is invalid (an attribute is invalid)
	ePvErrResources = 10,       // System/network resources or memory not available
	ePvErrBandwidth = 11,       // 1394 bandwidth not available
	ePvErrQueueFull = 12,       // Too many frames on queue
	ePvErrBufferTooSmall = 13,       // Frame buffer is too small
	ePvErrCancelled = 14,       // Frame cancelled by user
	ePvErrDataLost = 15,       // The data for the frame was lost
	ePvErrDataMissing = 16,       // Some data in the frame is missing
	ePvErrTimeout = 17,       // Timeout during wait
	ePvErrOutOfRange = 18,       // Attribute value is out of the expected range
	ePvErrWrongType = 19,       // Attribute is not this type (wrong access function) 
	ePvErrForbidden = 20,       // Attribute write forbidden at this time
	ePvErrUnavailable = 21,       // Attribute is not available at this time
	ePvErrFirewall = 22,       // A firewall is blocking the traffic (Windows only)
	__ePvErr_force_32 = 0xFFFFFFFF

} tPvErr;

struct tPvFrame
{
	//----- In -----
	void* ImageBuffer;        // Your image buffer
	unsigned long       ImageBufferSize;    // Size of your image buffer in bytes

	void* AncillaryBuffer;    // Your buffer to capture associated 
											//   header & trailer data for this image.
	unsigned long       AncillaryBufferSize;// Size of your ancillary buffer in bytes
											//   (can be 0 for no buffer).

	void* Context[4];         // For your use (valuable for your
											//   frame-done callback).
	unsigned long       _reserved1[8];

	//----- Out -----

	tPvErr              Status;             // Status of this frame

	unsigned long       ImageSize;          // Image size, in bytes
	unsigned long       AncillarySize;      // Ancillary data size, in bytes

	unsigned long       Width;              // Image width
	unsigned long       Height;             // Image height
	unsigned long		Channel;
	unsigned long       RegionX;            // Start of readout region (left)
	unsigned long       RegionY;            // Start of readout region (top)
	tPvImageFormat      Format;             // Image format
	unsigned long       BitDepth;           // Number of significant bits
	tPvBayerPattern     BayerPattern;       // Bayer pattern, if bayer format

	unsigned long       FrameCount;         // Rolling frame counter
	unsigned long       TimestampLo;        // Time stamp, lower 32-bits
	unsigned long       TimestampHi;        // Time stamp, upper 32-bits

	unsigned long       _reserved2[32];
	const void* GetImageBuffer_R() const { return ImageBuffer; }
	void* GetImageBuffer_W() { return ImageBuffer; }
	void SetImageBuffer(void* pBuffer) { ImageBuffer = pBuffer; }

};

tPvFrame* CreateFrameImageBuffer(int iChannel, int width, int height);

class Cfov2pngApp : public CWinApp
{
public:
	Cfov2pngApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern Cfov2pngApp theApp;
