
// fov2pngDlg.h : header file
//

#pragma once


// Cfov2pngDlg dialog
class Cfov2pngDlg : public CDialogEx
{
// Construction
public:
	Cfov2pngDlg(CWnd* pParent = nullptr);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_FOV2PNG_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	void Config_Init();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	bool CheckArgs();
	DECLARE_MESSAGE_MAP()
public:
	int width_;
	int height_;
	CString sourceDir_;
	CString destDir_;
	CString imgFormat_;
	afx_msg void OnBnClickedButton1();
	void Config_Save();
	int cannels_;
	CProgressCtrl m_progress;
	afx_msg void OnNMCustomdrawProgress1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnEnChangeEdit3();
	afx_msg void OnEnChangeEdit4();
	afx_msg void OnStnClickedStaticPro();

	afx_msg void OnEnChangeEdit2();
	CButton m_Transfer;
	afx_msg void OnCbnSelchangeCombo1();
	afx_msg void OnEnChangeEdit1();
	afx_msg void OnCbnSelchangeCombo2();
	CComboBox img_type;
	CComboBox img_cannel;
};
