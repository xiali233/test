
// fov2png.cpp : Defines the class behaviors for the application.
//

#include "pch.h"
#include "framework.h"
#include "fov2png.h"
#include "fov2pngDlg.h"
#include <exception>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Cfov2pngApp

BEGIN_MESSAGE_MAP(Cfov2pngApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// Cfov2pngApp construction

Cfov2pngApp::Cfov2pngApp()
{
	// support Restart Manager
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only Cfov2pngApp object

Cfov2pngApp theApp;


// Cfov2pngApp initialization

BOOL Cfov2pngApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();


	AfxEnableControlContainer();

	// Create the shell manager, in case the dialog contains
	// any shell tree view or shell list view controls.
	CShellManager *pShellManager = new CShellManager;

	// Activate "Windows Native" visual manager for enabling themes in MFC controls
	CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	Cfov2pngDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}
	else if (nResponse == -1)
	{
		TRACE(traceAppMsg, 0, "Warning: dialog creation failed, so application is terminating unexpectedly.\n");
		TRACE(traceAppMsg, 0, "Warning: if you are using MFC controls on the dialog, you cannot #define _AFX_NO_MFC_CONTROLS_IN_DIALOGS.\n");
	}

	// Delete the shell manager created above.
	if (pShellManager != nullptr)
	{
		delete pShellManager;
	}

#if !defined(_AFXDLL) && !defined(_AFX_NO_MFC_CONTROLS_IN_DIALOGS)
	ControlBarCleanUp();
#endif

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

tPvFrame* CreateFrameImageBuffer(int iChannel, int width, int height)
{
	tPvFrame* pFrameImage = nullptr;

	try
	{
		// FIXME: this looks awful.  use some proper image data class to deal
		// with the allocations

		unsigned long FrameSize = (unsigned long)(width * height);
		pFrameImage = new tPvFrame;
		pFrameImage->ImageBufferSize = (unsigned long)FrameSize * iChannel;
		pFrameImage->Format = ePvFmtRgb24;
		pFrameImage->_reserved1[7] = 1;
		pFrameImage->ImageBuffer = new BYTE[pFrameImage->ImageBufferSize];
		pFrameImage->Height = height;
		pFrameImage->Width = width;
		pFrameImage->Channel = iChannel;
		if (iChannel == 3)
			pFrameImage->Format = ePvFmtRgb24;
		else if (iChannel == 1)
			pFrameImage->Format = ePvFmtMono8;
	}
	catch (const std::bad_alloc& memExp)
	{
		CString strError(memExp.what());
		strError = _T("Alloc memory error: ") + strError;
		AfxMessageBox(strError);

		if (pFrameImage)
		{
			if (pFrameImage->ImageBuffer)
				delete[] pFrameImage->ImageBuffer;
			delete pFrameImage;
			pFrameImage = NULL;
		}
	}

	return pFrameImage;
}
