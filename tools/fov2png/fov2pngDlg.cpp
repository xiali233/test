
// fov2pngDlg.cpp : implementation file
//

#include "pch.h"
#include "framework.h"
#include "fov2png.h"
#include "fov2pngDlg.h"
#include "afxdialogex.h"
#include <cv.h>
#include <opencv2\imgcodecs\imgcodecs_c.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
public:
	
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
	
END_MESSAGE_MAP()


// Cfov2pngDlg dialog



Cfov2pngDlg::Cfov2pngDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_FOV2PNG_DIALOG, pParent)
	, width_(0)
	, height_(0)
	, sourceDir_(_T(""))
	, destDir_(_T(""))
	, imgFormat_(_T(""))
	, cannels_(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void Cfov2pngDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, width_);
	DDV_MinMaxInt(pDX, width_, 0, 100000);
	DDX_Text(pDX, IDC_EDIT2, height_);
	DDV_MinMaxInt(pDX, height_, 0, 100000);
	DDX_Text(pDX, IDC_EDIT3, sourceDir_);
	DDX_Text(pDX, IDC_EDIT4, destDir_);
	DDX_CBString(pDX, IDC_COMBO1, imgFormat_);
	DDX_CBIndex(pDX, IDC_COMBO2, cannels_);
	//	DDV_MinMaxInt(pDX, cannels_, 0, 3);
	DDX_Control(pDX, IDC_PROGRESS1, m_progress);
	DDX_Control(pDX, IDC_BUTTON1, m_Transfer);
	DDX_Control(pDX, IDC_COMBO1, img_type);
	DDX_Control(pDX, IDC_COMBO2, img_cannel);
}

BEGIN_MESSAGE_MAP(Cfov2pngDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &Cfov2pngDlg::OnBnClickedButton1)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_PROGRESS1, &Cfov2pngDlg::OnNMCustomdrawProgress1)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON2, &Cfov2pngDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &Cfov2pngDlg::OnBnClickedButton3)
	ON_EN_CHANGE(IDC_EDIT3, &Cfov2pngDlg::OnEnChangeEdit3)
	ON_EN_CHANGE(IDC_EDIT4, &Cfov2pngDlg::OnEnChangeEdit4)
	ON_STN_CLICKED(IDC_STATIC_Pro, &Cfov2pngDlg::OnStnClickedStaticPro)
	
	ON_EN_CHANGE(IDC_EDIT2, &Cfov2pngDlg::OnEnChangeEdit2)
	ON_CBN_SELCHANGE(IDC_COMBO1, &Cfov2pngDlg::OnCbnSelchangeCombo1)
	ON_EN_CHANGE(IDC_EDIT1, &Cfov2pngDlg::OnEnChangeEdit1)
	ON_CBN_SELCHANGE(IDC_COMBO2, &Cfov2pngDlg::OnCbnSelchangeCombo2)
END_MESSAGE_MAP()


// Cfov2pngDlg message handlers

BOOL Cfov2pngDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	Config_Init();
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void Cfov2pngDlg::Config_Init() {
	CStdioFile in_file;
	bool rel = in_file.Open(L"default.ini", CFile::modeRead);
	CString buf[6];
	CString temp;
	int cannels = 0;
	int type = 0;
	int i = 0;
	if (rel != false) {
		while (in_file.ReadString(temp) && i < 6)
		{
			if (temp == "\r")
				continue;
			buf[i] = temp;
			i++;
		}

		width_ = _ttoi(buf[0]);
		height_ = _ttoi(buf[1]);
		cannels = _ttoi(buf[2]);
		type = _ttoi(buf[3]);
		sourceDir_ = buf[4];
<<<<<<< HEAD
		destDir_ = buf[5];

=======
		destDir_ = buf[5] ;
>>>>>>> d41e95daba8453ee9062bb77edf18950445daf68
	}

	in_file.Close();
	m_progress.SetStep(1);
	UpdateData(0);
	img_type.SetCurSel(type);
	img_cannel.SetCurSel(cannels);
}

void Cfov2pngDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void Cfov2pngDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR Cfov2pngDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}




bool Cfov2pngDlg::CheckArgs()
{
	bool ret = true;
	if (sourceDir_.Right(1) == L"\\" |sourceDir_.Right(1) == L"\r")
		sourceDir_ = sourceDir_.Left(sourceDir_.GetLength() - 1);

	if( destDir_.Right(1) == L"\\"| destDir_.Right(1) == L"\r")
		destDir_ = destDir_.Left(destDir_.GetLength() - 1);
		return ret;
	
	
}








void Cfov2pngDlg::OnBnClickedButton1()
{
	UpdateData(1);
	int sum = 0, count = 0;
	CString str_progressshow;
	CFileFind find;
	
	
	Config_Save();
	bool ret = find.FindFile(sourceDir_ + L"\\*.*");
	m_progress.SetPos(0);
	
	while (ret)
	{
		ret = find.FindNextFileW();//找到最后一个文件时返回零
		if (find.IsDots() || find.IsDirectory() || find.GetFileTitle().Find(L'-') != -1) // 找到的文件不为目录且文件名无“-”
			continue;
		sum++;
	}
	m_progress.SetRange(0, sum);   // 根据符合条件文件数设置进度条长度
	

	
	int cannel = 0;
	if (cannels_ == 0)
		cannel = 1;
	else if (cannels_ == 1)
		cannel = 3;
	
	if (CheckArgs())
	{
		if (find.FindFile(sourceDir_ + L"\\*.*") )
			
		{
			bool found = false;
			do {
				
				found = find.FindNextFileW();
				auto path = find.GetFilePath();
				if (find.IsDots() || find.IsDirectory() || find.GetFileTitle().Find(L'-') != -1) // 找到的文件不为目录且文件名无“-”
					continue;
				CFile f(path, CFile::modeRead);

				auto pFrame = CreateFrameImageBuffer(cannel, width_, height_);

				f.Read(pFrame->ImageBuffer, pFrame->ImageBufferSize);
				f.Close();
				IplImage* image = cvCreateImage(CvSize(width_, height_), 8, cannel);
				memcpy(image->imageData, pFrame->ImageBuffer, pFrame->ImageBufferSize);
				cvSaveImage((CStringA)(destDir_ + L"\\" + find.GetFileTitle() + L"." + imgFormat_), image);
<<<<<<< HEAD
=======
				
>>>>>>> d41e95daba8453ee9062bb77edf18950445daf68
				delete pFrame->ImageBuffer;
				cvReleaseImage(&image);
				m_progress.StepIt();
				count++;
				str_progressshow.Format(L"%d//%d", count,sum);
				//Sleep(100 0); // 测试进度条功能是否正常
				SetDlgItemText(IDC_STATIC_Pro, str_progressshow);
				
			} while (found);

			
		}
	}

	find.Close();
	if (count == sum&&count!=0) {
		::MessageBox(NULL, L"转换完成", L"转换提示", MB_OK);
	}
	
}

void Cfov2pngDlg::Config_Save()
{
	CString width;
	CString height;
	CString str_cannel;
	CString type_img;
	CStdioFile out_file;
	out_file.Open(L"default.ini", CFile::modeCreate);
	out_file.Close();
	width.Format(L"%d", width_);
	height.Format(L"%d", height_);
	str_cannel.Format(L"%d", cannels_);
	type_img.Format(L"%d", img_type.GetCurSel());
	out_file.Open(L"default.ini", CFile::modeWrite || CFile::modeCreate || CFile::modeNoTruncate);


<<<<<<< HEAD
	out_file.WriteString(width + L"\n");
	out_file.WriteString(height + L"\n");
	out_file.WriteString(str_cannel + L"\n");
	out_file.WriteString(type_img + L"\n");
	out_file.WriteString(sourceDir_ + L"\n");
	out_file.WriteString(destDir_ + L"\n");
=======
	out_file.WriteString(width + L"\r\n");
	out_file.WriteString(height + L"\r\n");
	out_file.WriteString(str_cannel + L"\r\n");
	out_file.WriteString(type_img + L"\r\n");
	out_file.WriteString(sourceDir_+ L"\r\n");
	out_file.WriteString(destDir_ + L"\r\n");
>>>>>>> d41e95daba8453ee9062bb77edf18950445daf68


	out_file.Close();
	m_Transfer.EnableWindow(0);

}

void Cfov2pngDlg::OnNMCustomdrawProgress1(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
}





void Cfov2pngDlg::OnBnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码

	// 配置对话框

	BROWSEINFO bi;
	ZeroMemory(&bi, sizeof(bi));
	
	 
    bi.ulFlags = BIF_EDITBOX | BIF_RETURNONLYFSDIRS; // 可以输入搜索文档|仅返回文件系统目录
	
	//bi.lpszTitle=L"Specifies the folder or file to deal with";       //在窗口内显示提示用户的语句  和标题类似
	
	// 打开对话框, 有点像DoModal
	LPITEMIDLIST sourceLocation = SHBrowseForFolder(&bi);
	if (sourceLocation != NULL) {
		TCHAR source_Path[MAX_PATH];
		SHGetPathFromIDList(sourceLocation, source_Path);
		//MessageBox( sourcePath );
		CWnd* cwnd = GetDlgItem(IDC_EDIT3);
		cwnd->SetWindowText(source_Path);//将路径显示
	}
	m_Transfer.EnableWindow(1);
}


void Cfov2pngDlg::OnBnClickedButton3()
{
	// TODO: 在此添加控件通知处理程序代码
	
	
	// 配置对话框
	
	BROWSEINFO bi;
	ZeroMemory(&bi, sizeof(bi));

	//bi.lpszTitle = _T("Save Folder"); // 选择文件夹的标题
	//bi.ulFlags = BIF_EDITBOX ; // 搜索文件功能和新建文件夹功能冲突予以舍弃
	bi.ulFlags = BIF_RETURNONLYFSDIRS| BIF_NEWDIALOGSTYLE; // 仅返回文件系统目录|窗口可以调整大小，有新建文件夹按钮
	// 打开对话框, 有点像DoModal
	LPITEMIDLIST destLocation = SHBrowseForFolder(&bi);
	if (destLocation != NULL) {
		TCHAR dest_Path[MAX_PATH];
		SHGetPathFromIDList(destLocation, dest_Path);
		CWnd* cwnd = GetDlgItem( IDC_EDIT4);
		cwnd ->SetWindowText(dest_Path);// 将路径显示
	}
	
	m_Transfer.EnableWindow(1);
}


void Cfov2pngDlg::OnEnChangeEdit3()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CDialogEx::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码
	m_Transfer.EnableWindow(1);
}


void Cfov2pngDlg::OnEnChangeEdit4()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CDialogEx::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码
	m_Transfer.EnableWindow(1);
}


void Cfov2pngDlg::OnStnClickedStaticPro()
{
	// TODO: 在此添加控件通知处理程序代码
}





void Cfov2pngDlg::OnEnChangeEdit2()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CDialogEx::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码
	m_Transfer.EnableWindow(1);
}




void Cfov2pngDlg::OnCbnSelchangeCombo1()
{
	// TODO: 在此添加控件通知处理程序代码
	m_Transfer.EnableWindow(1);
}


void Cfov2pngDlg::OnEnChangeEdit1()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CDialogEx::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。
	
	// TODO:  在此添加控件通知处理程序代码
	m_Transfer.EnableWindow(1);
}


void Cfov2pngDlg::OnCbnSelchangeCombo2()
{
	// TODO: 在此添加控件通知处理程序代码
	m_Transfer.EnableWindow(1);
	

}
